package school;

public class Student extends Person{
    private String course;
    private int registration;
    
    public void payment(){
        System.out.println("Paied!");
    }
    
    public void setCourse(String course){
        this.course = course;
    }
    
    public String getCourse(){
        return this.course;
    }
    
    public void setRegistration(int registration){
        this.registration = registration;
    }
    
    public int getRegistration(){
        return this.registration;
    }
    
    public String toString(){
        return "{name: " + this.getName() + " |Gender:" + this.getGender() +
                " |Age: " + this.getAge() + " |Course: " + this.getCourse() + 
                " |Regist: " + this.getRegistration() + " }";
    }
}
