package school;

public class Professor extends Person{
   private String specialization;
   private int salary;
   
   public void setSpecialization(String specialization){
       this.specialization = specialization;
   }
   
   public String getSpecialization(){
       return this.specialization;
   }
   
   public void setSalary(int salary){
       this.salary = salary;
   }
   
   public int getSalary(){
       return this.salary;
   }
   
   public void increaseSalary(int qtd){
       this.setSalary(this.getSalary() + qtd);
   }
}
