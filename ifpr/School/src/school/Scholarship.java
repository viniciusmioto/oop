package school;

public class Scholarship extends Student{
    public int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Scholarship{" + "value=" + value + " Name: " + this.getName() +
                " Course: " + this.getCourse() + '}';
    }
    
    @Override
    public void payment(){
        System.out.println(this.getName() + " is a scholarship, payment ok.");
    }
}
