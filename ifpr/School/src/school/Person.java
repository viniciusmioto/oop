package school;

public abstract class Person {
    private String name;
    private int age;
    private char gender;
    
    public void birthday(){
        this.age += 1;
    }
    
    public String toString(){
        return "{name: " + this.getName() + " Age: " + this.getAge() +
                " Gender: " + this.getGender() + " }";
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setAge(int age){
        this.age = age;
    }
    
    public int getAge(){
        return this.age;
    }
    
    public void setGender(char gender){
        this.gender = gender;
    }
    
    public char getGender(){
        return gender;
    }
    
}
