package school;

public class School {
    public static void main(String[] args) {
        Student s1 = new Student();
        s1.setName("Vinicius");
        s1.setGender('M');
        s1.setAge(18);
        s1.birthday();
        s1.setCourse("Computer Science");
        s1.setRegistration(805);
        s1.payment();
        System.out.println(s1.toString() + "\n");
        
        Scholarship b1 = new Scholarship();
        b1.setName("Mary");
        b1.setGender('F');
        b1.setAge(21);
        b1.setCourse("Economics");
        b1.payment();
        b1.setValue(5550);
        System.out.println(b1.toString());
        
    }
    
}
