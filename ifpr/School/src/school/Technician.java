package school;

public class Technician extends Student{
    private int id;
    
    public void setId(int id){
        this.id = id;
    }
    
    public int getId(){
        return this.id;
    }
    
    public void practice(){
        System.out.println("Getting better...");
    }
    
    @Override
    public String toString(){
        return "{name: " + this.getName() + " age: " + this.getAge() +
                " id: " + this.getId() + " }";
    }
}
